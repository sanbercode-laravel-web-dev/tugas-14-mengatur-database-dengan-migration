@extends('layout.master')

@section('title')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome-page" method="post">
    @csrf
    <div>
        <label for="first_name">First Name : </label><br/><br/>
        <input type="text" name="first_name" id="first_name"><br/><br/>
    </div>
    <div>
        <label for="last_name">Last Name : </label><br/><br/>
        <input type="text" name="last_name" id="last_name"><br/><br/>
    </div>
    <div>
        <label for="gender">Gender : </label><br/><br/>
        <input type="radio" id="male" name="gender" value="1">
        <label for="male">Male</label><br/>
        <input type="radio" id="female" name="gender" value="2">
        <label for="female">Female</label><br/>
        <input type="radio" id="other" name="gender" value="3">
        <label for="other">Other</label><br/><br/>
    </div>
    <div>
        <label for="nationality">Nationality : </label><br/><br/>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporeans">Singapore</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br/><br/>
    </div>
    <div>
        <label for="language">Language Spoken : </label><br/><br/>
        <input type="checkbox" id="language_indonesia" name="language[]" value="language_indonesia">
        <label for="language_indonesia"> Bahasa Indonesia</label><br/>
        <input type="checkbox" id="language_english" name="language[]" value="language_english">
        <label for="language_english"> English</label><br/>
        <input type="checkbox" id="language_other" name="language[]" value="language_other">
        <label for="language_other"> Other</label><br/><br/>
    </div>
    <div>
        <label for="bio">Bio : </label><br/><br/>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
    </div>
    <input type="submit" value="Sign Up">
</form>
@endsection

